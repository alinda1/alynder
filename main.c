#include <stdio.h>

#include <math.h>

int main()
{
    float a, b, c, discriminant, root1, root2;
    printf("Enter coefficients a, b, and c:");
    scanf("%f,%f,%f",&a, &b, &c);
    if (a==0)
    {
        printf("Invalid input! Coefficient ,a, cannot be 0.");
        return 0;
    }

    discriminant = b*b-4*a*c;

    if (discriminant==0)
    {
        root1 = -b/(2*a);
        root2 = -b/(2*a);
        printf("Roots are real ans equal\n");
        printf("root 1 = root 2 = %f\n",root1);

    }
    else if (discriminant>0)
    {
        root1 = (-b+sqrt(discriminant))/(2*a);
        root2 = (-b-sqrt(discriminant))/(2*a);
        printf("Roots are real and distinct\n");
        printf("root 1=%f\n",root1);
        printf("root 2=%f\n",root2);
    }
    else
    {
        printf("Complex");
    }
    return 0;
}
